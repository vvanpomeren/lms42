import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

abstract public class CrackerSequential {

    static final int MAX_PASSWORD_LENGTH = 5;
    static final String secretPassword = "zippy";

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException, ExecutionException, InterruptedException {
        // Generate and display the hash
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        byte[] hash = digest.digest(secretPassword.getBytes(StandardCharsets.UTF_8));
        System.out.println("Password hash: " + bytesToHex(hash));

        // Brute-force the hash
        long before = System.currentTimeMillis();
        String result = crack(hash);
        long after = System.currentTimeMillis();

        // Show the results
        if (result == null) {
            System.out.println("Password not recovered.");
        } else {
            System.out.println("Your password is: " + result);
        }
        System.out.println("Execution time: " + (after - before) + "ms");
    }

    /** Convert a byte array to a hexadecimal String.
     * @param hash Input byte array.
     * @return Hexadecimal representation of the input hash.
     */
    static String bytesToHex(byte[] hash) {
        return new BigInteger(1, hash).toString(16);
    }

    /**
     * Convert a List of bytes to a String using UTF-8 decoding.
     * @param data The input list of bytes.
     * @return The UTF-8 String.
     */
    static String byteListToString(List<Byte> data) {
        byte[] bytesArray = new byte[data.size()];
        for(int i = 0; i < data.size(); i++) {
            bytesArray[i] = data.get(i);
        }
        return new String(bytesArray, StandardCharsets.UTF_8);
    }

    /** Try to recover a SHA1'd password by brute forcing all combinations involving characters a-z up
     * to MAX_PASSWORD_LENGTH characters.
     * @param hash The SHA1 hash that we are seeking to match.
     * @param guess A list of bytes that all passwords to try should start with. Can be empty to check all password.
     * @return Returns the password, if found, or null otherwise.
     */
    static String crackRecursive(byte[] hash, ArrayList<Byte> guess)  {
        // Create a SHA1 digester
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        // Calculate the digest for this password guess
        for(Byte b : guess) digest.update(b);
        byte[] hash2 = digest.digest();
        // If it's equal to the hash, return the guessed password
        if (Arrays.equals(hash, hash2)) {
            return byteListToString(guess);
        }

        // If the current guess exceeds to max length, no need for further recursion
        if (guess.size() >= MAX_PASSWORD_LENGTH) return null;

        // Recursive case: add another letter
        for (byte ch = 'a'; ch <= 'z'; ch++) {
            guess.add(ch);
            String result = crackRecursive(hash, guess);
            if (result != null) return result;
            guess.removeLast();
        }
        return null;
    }

    /** Try to recover a SHA1'd password by brute forcing all combinations involving characters a-z up
     * to MAX_PASSWORD_LENGTH characters.
     * @param hash The SHA1 hash that we are seeking to match.
     * @return Returns the password, if found, or null otherwise.
     */
    static String crack(byte[] hash) {
        return crackRecursive(hash, new ArrayList<>());
    }

}
