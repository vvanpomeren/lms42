import asyncio

PORT = 6666


async def handle_connection(reader, writer):
    await send(writer, "Welcome to the echo chamber")
    try:
        while True:
            line = await reader.readline()
            if len(line) == 0: # connection terminated
                break
                
            line = str(line, "utf-8").rstrip("\r\n")
            if line == "exit":
                break
            await send(writer, line)
    finally:
        await clean_up(writer)


async def send(writer, message):
    writer.write(bytes(message+"\n", "utf-8"))
    await writer.drain()


async def clean_up(writer):
    writer.close()
    await writer.wait_closed()


async def main():
    server = await asyncio.start_server(handle_connection, '0.0.0.0', PORT)
    await server.wait_closed()


asyncio.run(main())
    
