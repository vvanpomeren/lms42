name: Open Source Project preparation
description: Choose an Open Source project and select/create one or more issues you'd like to work on.
public_for_students: true
grading: false
goals:
    legacy_analyze: 1
    legacy_impl: 1
    legacy_contrib: 1

assignment:
    - Open Source Project:
        - This assignment is preparation for the Open Source Project. The goal is to come up with an approach for this project, and to check it with a teacher before you actually start.

    - Warning:
        - Don't get trapped into spending too much time with this. Just make a decision and submit this assignment by the end of your working day."

    -   Find an Open Source project:
        -
            link: https://goodfirstissue.dev/
            title: Good First Issue
            info: A database of open source projects that have a curated list of good first issues to tackle.
        -
            link: https://github.com/MunGell/awesome-for-beginners
            title: Awesome for Beginners
            info: Another list of projects with beginner friendly issues.
        - |
            The Open Source project should fulfill the following requirements:

            - Contains at least 15k lines of code. [This site](https://codetabs.com/count-loc/count-loc-online.html) can help you count.
            - Has been updated in the last two months.

            You will probably want to pick a project that you're pretty familiar with (or can easily understand) as an end user.

            **Advice:** Some students get stuck in *analysis paralysis* while selecting a project and a first issue. Don't be like them and use [timeboxing](https://en.wikipedia.org/wiki/Timeboxing). Set yourself a fixed amount of time (4 hours for instance) after which you'll commit to work on the best alternative you've come across. While exploring projects, always have your current best alternative written down somewhere. When evaluating the next project, you'll only need to decide if it's better than the current best.

    -   Find the coding standards for the project:
        - Find the coding standards for the project you want to contribute to. If the project doesn't explicitly specify a standard, try to find a public standard that matches the practices used by the project.


    -   Find or create issues a first issue want to work on in this project:
        - |
            Find a simple first issue in the project that you want to work on. Look for issues labeled "good first issue" or "beginner". Your first issue does not need to be large enough to fill the entire 7 day project, but should be enough to get you engaged with the project. Finding suitable further (larger) issues should be easier after that.

            You can also come up with an issue of your own for the project. This can either be a bug that you want to resolve or an improvement you want to add to the project. Make sure you create a GitHub *issue* and actively ask other contributors for opinions before starting to implement your idea.

            In case of doubt if an issue is suitable: ask a teacher and/or extra nicely ask the maintainers (or 'core contributors') for advice.

    - Submitting:
        -
            must: true
            text: |
                Create and submit a file that includes links to the project, the issues and any relevant documentation (including any coding standards and/or code of conduct).

                During feedback, you'll discuss the viability of your plans with the teacher.
