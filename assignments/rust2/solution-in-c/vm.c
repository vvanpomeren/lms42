// Build using:
// gcc -g -O3 vm.c -o vm

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

typedef uint16_t PTR_TYPE;
const int PTR_SIZE = sizeof(PTR_TYPE);

uint8_t memory[64 * 1024];
char *labels[64 * 1024];

const char *instrs[] = {"INVALID", "set", "add", "sub", "mul", "div", "and", "or", "xor", "if=", "if>", "if<", "read", "write"};

uint32_t load(uint32_t addr, int size)
{
	if (size == 1)
	{
		return *(uint8_t *)(memory + addr);
	}
	else if (size == 2)
	{
		return *(uint16_t *)(memory + addr);
	}
	else
	{
		return *(uint32_t *)(memory + addr);
	}
}

void store(uint32_t addr, uint32_t value, int size)
{
	if (size == 1)
	{
		*(uint8_t *)(memory + addr) = (uint8_t)value;
	}
	else if (size == 2)
	{
		*(uint16_t *)(memory + addr) = (uint16_t)value;
	}
	else
	{
		*(uint32_t *)(memory + addr) = (uint32_t)value;
	}
}

void read_debug_file(char *exeFilename)
{
	FILE *filePointer;
	const int bufferLength = 255;
	char buffer[bufferLength];
	char filename[strlen(exeFilename)+1];
	strcpy(filename, exeFilename);
	char *point = strstr(filename, ".executable");
	strcpy(point, ".debug");

	filePointer = fopen(filename, "r");
	if (filePointer != NULL)
	{
		while (fgets(buffer, bufferLength, filePointer))
		{
			int address;
			int label_length = strchr(buffer, '\n') - buffer - 5;
			sscanf(buffer, "%04x", &address);
			char *label = (char*)malloc(label_length + 1);
			strncpy(label, buffer + 5, label_length);
			label[label_length] = '\0';
			labels[address & 0xffff] = label;
			// printf("%04x:%s\n", address, label);
		}
		fclose(filePointer);
	}
}

void print_with_label(PTR_TYPE addr)
{
	fprintf(stderr, "[%04x", addr);
	char *label = labels[addr & 0xffff];
	if (label)
		fprintf(stderr, ":%s", label);
	fputc(']', stderr);
}

void print_value(uint8_t wordSize, uint32_t value)
{
	switch (wordSize)
	{
	case 1:
		fprintf(stderr, "%02x", value & 0xff);
		break;
	case 2:
		value &= 0xffff;
		fprintf(stderr, "%04x", value);
		char *label = labels[value];
		if (label)
			fprintf(stderr, ":%s", label);
		break;
	case 4:
		fprintf(stderr, "%08x", value & 0xffffffff);
	}
}

int main(int argc, char *argv[])
{
	uint8_t debug = 0;
	uint8_t single_step = 0;
	int argPos = 1;
	if (argc >= 2)
	{
		if (!strcmp(argv[1], "-d"))
		{
			debug = 1;
			argPos++;
		}
		else if (!strcmp(argv[1], "-s"))
		{
			debug = 1;
			single_step = 1;
			argPos++;
		}
	}
	if (argc != argPos + 1)
	{
		fprintf(stderr, "Syntax: %s [-d] or [-s] <program_file>\n", argv[0]);
		return 1;
	}

	int fd = open(argv[argPos], O_RDONLY);
	if (fd < 0)
	{
		fprintf(stderr, "Couldn't open program file: %s\n", argv[argPos]);
		return 2;
	}
	read(fd, memory, sizeof(memory));
	close(fd);

	read_debug_file(argv[argPos]);

	int skip = 0;
	PTR_TYPE *ipPtr = (PTR_TYPE *)&memory[0];

	while (1)
	{
		uint16_t startIp = *ipPtr;
		if (*ipPtr == 0)
		{
			if (debug)
				fprintf(stderr, "SYSTEM HALTED\n");
			break; // halt
		}
		uint16_t ip = startIp;

		uint8_t opcode = load(ip++, 1);
		uint8_t argSpec = load(ip++, 1);

		uint8_t wordSize = argSpec & 3;
		if (wordSize == 0)
			wordSize = 1;
		else if (wordSize == 1)
			wordSize = 2;
		else if (wordSize == 2)
			wordSize = 4;
		argSpec >>= 2;

		uint8_t argTypes[2];
		void *argPtrs[2];
		uint32_t args[2];

		for (int argNum = 0; argNum < 2; argNum++)
		{
			argTypes[argNum] = argSpec & 3;
			argSpec >>= 2;
			switch (argTypes[argNum])
			{
			case 0:
				args[argNum] = 0;
				argPtrs[argNum] = &args[argNum];
				break;
			case 1:
				argPtrs[argNum] = memory + ip;
				ip += wordSize;
				break;
			case 2:
				argPtrs[argNum] = memory + load(ip, PTR_SIZE);
				ip += PTR_SIZE;
				break;
			case 3:
				argPtrs[argNum] = memory + load(load(ip, PTR_SIZE), PTR_SIZE);
				ip += PTR_SIZE;
				break;
			}

			switch (wordSize)
			{
			case 1:
				args[argNum] = *(uint8_t *)argPtrs[argNum];
				break;
			case 2:
				args[argNum] = *(uint16_t *)argPtrs[argNum];
				break;
			case 4:
				args[argNum] = *(uint32_t *)argPtrs[argNum];
				break;
			}
		}

		*ipPtr = ip;

		if (skip > 0)
		{
			skip = 0;
			continue;
		}

		if (debug && opcode < sizeof(instrs) / sizeof(instrs[0]))
		{
			print_with_label(startIp);
			fprintf(stderr, ": %s%d ", instrs[opcode], 8 * wordSize);
			print_value(wordSize, args[0]);
			if (argPtrs[0] != &args[0] && argTypes[0] > 1)
				print_with_label((PTR_TYPE)((uint8_t *)argPtrs[0] - memory));
			fputc(' ', stderr);
			print_value(wordSize, args[1]);
			if (argPtrs[1] != &args[1] && argTypes[1] > 1)
				print_with_label((PTR_TYPE)((uint8_t *)argPtrs[1] - memory));
		}

		switch (opcode)
		{
		case 1:
			args[0] = args[1];
			break;
		case 2:
			args[0] += args[1];
			break;
		case 3:
			args[0] -= args[1];
			break;
		case 4:
			args[0] *= args[1];
			break;
		case 5:
			args[0] /= args[1];
			break;
		case 6:
			args[0] &= args[1];
			break;
		case 7:
			args[0] |= args[1];
			break;
		case 8:
			args[0] ^= args[1];
			break;
		case 9:
			skip = (args[0] == args[1]) ? -1 : 1;
			break;
		case 10:
			skip = (args[0] > args[1]) ? -1 : 1;
			break;
		case 11:
			skip = (args[0] < args[1]) ? -1 : 1;
			break;
		case 12:
		{
			int ch = fgetc(stdin);
			args[0] = ch == EOF ? 256 : ch;
			break;
		}
		case 13:
			fputc(args[0] & 255, stdout);
			break;
		default:
			fprintf(stderr, "Invalid opcode %x at %04x\n", (int)opcode, startIp);
			return 1;
		}

		if (skip != 0)
		{
			if (debug)
				fprintf(stderr, skip > 0 ? " = FALSE\n" : " = TRUE\n");
			if (skip < 0)
				skip = 0;
		}
		else
		{
			switch (wordSize)
			{
			case 1:
				*(uint8_t *)argPtrs[0] = args[0];
				break;
			case 2:
				*(uint16_t *)argPtrs[0] = args[0];
				break;
			case 4:
				*(uint32_t *)argPtrs[0] = args[0];
				break;
			}
			if (debug)
			{
				fprintf(stderr, " = ");
				print_value(wordSize, args[0]);
				fputc('\n', stderr);
			}
		}

		if (single_step)
			fgetc(stdin);
	}

	return 0;
}
