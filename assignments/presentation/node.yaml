name: Five minute pitch
goals:
    presentation: 1
days: 1
allow_longer: true
type: presentation
upload: false
public_for_students: true
avg_attempts: 1
resources:
    -
        link: https://www.youtube.com/watch?v=BmEiZadVNWY
        title: TEDxEindhoven - How to present to keep your audience’s attention
        topics: MLK, ditch powerpoint, ask questions, tell stories
    -
        link: https://www.youtube.com/watch?v=K0pxo-dS9Hc
        title: TEDxZagreb - The 110 techniques of communication and public speaking
        topics: body language, pace, pauses, uhh.
    -
        link: https://www.youtube.com/watch?v=8S0FDjFBj8o
        title: How to sound smart in your TEDx Talk - Will Stephen

assignment: 
    Assignment: |
        <div class="notification is-warning">Please note that this assignment counts as part of the module exam.</div> 

        Give a short (at least 3 minutes, at most 5 minutes) presentation about either your <em>Open Source Project</em> or your <em>LMS42</em> work, in front of all students as part of the 12:00 ten-minute-lecture series. Your pitch should...

        - Explain the problem/opportunity.
        - Show what you've created, by means of a demo or screenshots or video. Impress upon the audience what software the world now has that it didn't have before you came along.
        - Explain how you did it.
        - Be in English.
        - Be well-prepared. Because you only have a couple of minutes, you want to come straight to the point. Though there's always time for a bit of humour. Also, test the TV or beamer in advance, so you don't waste your time (and the audience's attention) on technical problems.

        You *may* use slides, as long as they really add something that you're not also telling.

    Grading:    
    -
        title: Presentation
        map:
            presentation: 1
        text: |
            - Be clear.
            - Be confident.
            - Start with a strong hook.
            - Maintain the audience's interest using some of the techniques explained in the resources.
        0: Hesitant, unintelligible and dreadfully boring.
        4: Clear, confident, engaging and connecting with the audience.

    -
        title: Slides and/or demo
        map:
            presentation: 0.5
        text: |
            Your slides and/or demo should be well-prepared, look nice, and help support your pitch (instead of making your pitch superfluous).
        3: As described, like a reasonably prepared amateur. 
        4: As described, like a pro.
    
    -
        link: https://www.youtube.com/watch?v=GP1ww_1AJzI
        title: "How to Give Constructive and Actionable Peer Feedback: Students to Students"
    -
        title: Feedback
        must: true
        text: |
            For the next two presentations for this assignment by classmates, send the presenter an email with your detailed feedback and tips on her/his presentation, with a *cc* to the day's teacher. The goal is to help them improve, while passively training your own presentation skills as well.
            
            You don't need to delay submitting this assignment until you have provided this feedback. We assume that you will do this to the best of your abilities, when these presentation happen. (And if not, we can always retract your grade.)
