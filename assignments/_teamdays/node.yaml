name: End-of-quarter project
goals:
    teamwork: 1
description: A two-day team project.
days: 2
public_for_students: true
grading: false
avg_attempts: 1
ignore_previous_nodes: true

assignment:
    End-of-quarter project: |
        During the last two days of octants 2, 4 and 6, a fun and instructive event will be organized to celebrate all of the progress we've all made over the preceding ten weeks.

        In teams consisting of 4 or 5 students, you'll be challenged to use the limited time to create an awesome application!

        <div class="notification is-warning">
            Please note that this assignment counts as part of the module exam.
        </div>

    Planned editions: |
        | Octant | Start&nbsp;date | Days |
        | -- | -- | -- |
        | 6 | 2024‑04‑25 | Thursday and Friday |
        | 2 | 2024‑11‑14 | Thursday and Friday |
        | 4 | 2025‑02‑06 | Thursday and Friday |
        | 6 | 2024‑04‑24 | Thursday and Friday |

    Teams: |
        Teams will be created by the teachers, based on your current progress in the curriculum, putting people with similar skill levels together. Assignments will be constructed such that they can be worked on at different levels. For example one team could create a console-based Python application, while another creates a Flask application backed by a database, and yet another creates an Android or Svelte app communicating with a REST API backend.
        
        We will announce the teams a few days before the project starts, so you can come up with ideas on how to collaborate.

    Demo show: |
        Each End-of-quarter project concludes with a *demo show*, during which all teams will show off their awesome applications. For some editions, we'll require teams to create a short demo video for this, which we'll watch together. Other editions, we'll organize a *demo market*, where you can browse around and interact with the software created by other teams. Whatever the setup, we want the *demo show* to be fun. Oh, and there will be prizes in three categories! Best functionality. Best looks and polish. Most original.

    Schedule: |
        | Day | Time | What |
        | -- | -- | -- |
        | One | 9:00 | Kick-off & assignment briefing |
        | Two | 15:30 | Deadline, demo show & drinks! |

    Passing criteria:
    - |
        Passing an End-of-quarter project is intended to be sort-of a given, as long as you're putting in the effort and collaborating with your teammates. It's the experience that counts. To prevent unpleasant surprises, please review the criteria below before the start of the project.

    -
        must: true
        title: Deliverables
        text: |
            Upload a completed `report.md` and the product of your team's work. You can do this the Monday after the event, if you prefer.

    -
        must: true
        title: Team result
        text: |
            The team has delivered a product that is acceptable given the limited time and the team members' current progress in the curriculum.
            
            *Or* the team has failed to deliver such a product, despite a serious effort, and learned a lot in the process.

    -
        must: true
        title: Collaboration
        text: |
            The student coordinated her/his work with the rest of the team. Collaboration may not have been flawless, but a serious effort has been made.

    -
        must: true
        title: Fair share
        text: |
            The student has contributed his/her fair share to the accomplishments of the team.

            We expect student to at least put in the appropriate amount of time and effort.

    -
        must: true
        title: Reflection
        text: |
            The student makes it clear what she/he has learned from this collaboration experience.
