# Individual Project Reports

This report should take you about half an hour. You should write it alone, without your teammates looking over your shoulder.


## Who did what?

For each of your team mates (yourself included), describe in one or two sentences what he/she contributed to the project. You may also add a note if he/she collaborated especially good or bad.

**Name 1:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
**Name 2:** Etc...


## Collaboration failures?

Mention the most important things that went wrong collaborating. For each, propose how you would collaborate differently on your next (different!) project, to prevent this from happening again.

**Problem:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
**How to prevent:** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

**Problem:** Etc..
**How to prevent:** Etc..


## Collaboration successes?

Especially in case you didn't have much to say in the previous section, please mention the most important things that went right during the project collaboration.

**Success:** Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

**Success:** Etc..
