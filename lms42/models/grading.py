from ..app import db
from ..routes import discord
from .. import utils
import sqlalchemy as sa
import datetime
import flask
import os


class Grading(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)

    attempt_id = sa.Column(sa.Integer, sa.ForeignKey('attempt.id'), nullable=False, index=True)
    attempt = sa.orm.relationship("Attempt", backref=sa.orm.backref("gradings", lazy="dynamic"))

    time = sa.Column(sa.DateTime, default=datetime.datetime.utcnow, nullable=False)

    grader_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, index=True)
    grader = sa.orm.relationship("User", foreign_keys="Grading.grader_id")

    grade = sa.Column(sa.Integer, nullable=False)
    grade_motivation = sa.Column(sa.String)

    passed = sa.Column(sa.Boolean, nullable=False)

    objective_scores = sa.Column(sa.ARRAY(sa.Float), nullable=False)
    objective_motivations = sa.Column(sa.ARRAY(sa.String))

    needs_consent = sa.Column(sa.Boolean, nullable=False, default=False)
    consent_user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    consenter = sa.orm.relationship("User", foreign_keys="Grading.consent_user_id")
    consent_time = sa.Column(sa.DateTime)

    def send_grade_dm(self):
        student = self.attempt.student
        if self.attempt.status == "passed":
            content = f"You passed {self.attempt.module_name} with a {round(self.grade)}!"
        else:
            content = f"You failed {self.attempt.module_name} with a {round(self.grade)}."

        if student.discord_id and student.discord_notification_exam_reviewed:
            discord.send_dm(discord_id=student.discord_id, title=f"An exam has been reviewed by {self.grader.short_name}.", content=content)

    def send_consent_dm(self):
        # Send dms to all teachers.
        teachers = User.query \
                        .filter_by(is_active=True, is_hidden=False, level=50) \
                        .filter(User.id != self.grader_id) \
                        .all()
        content = f"Exam {self.attempt.node_name} by {self.attempt.student.short_name} needs grading consent."
        for teacher in teachers:
            if teacher.discord_id:
                discord.send_dm(discord_id=teacher.discord_id, title="Exam grading", content=content)

    def announce(self):
        if self.attempt.credits > 0:
            self.send_grade_dm() # Send a dm to the student with the passed grade.
            flask.flash(f"Remember to publish the grade in [Bison](https://saxion.educator.eu/judgment?identifier=beoordelingperstudent_saxion): a {self.grade} for {self.attempt.student.full_name} ({self.attempt.student.student_number}) regarding {self.attempt.module_name} submitted on {utils.utc_to_display(self.attempt.submit_time)}. Don't finalize it yet.")


from .user import User
