"""empty message

Revision ID: 15d005e78012
Revises: 769c010b9d15
Create Date: 2024-02-27 20:18:49.057044

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '15d005e78012'
down_revision = '769c010b9d15'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('english', sa.Boolean(), nullable=False, server_default='false'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'english')
    # ### end Alembic commands ###
